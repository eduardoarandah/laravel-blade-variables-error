<?php
namespace App\View\Components;

use Illuminate\View\Component;

class ChildComponent extends Component
{
    public $depth;
    public function __construct($depth)
    {
        $this->depth = $depth;
    }
    public function render()
    {
      // doesn't work:
        // return view('components.child-component');
        // does work:
        return view('components.child-component', [
          'depth' => $this->depth,
        ]);
    }
}
