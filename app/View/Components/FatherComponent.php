<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\View\Components\ChildComponent;

class FatherComponent extends Component
{
    public function render()
    {
        $children = $this->renderChildren(8);
        return view('components.father-component', [
          'children' => $children,
        ]);
    }

    public function renderChildren($depth)
    {
        $depth--;

       // exit condition
        if (!$depth) {
            return null;
        }

        $child = new ChildComponent($depth);
        $out = $child->render();
        $out .= $this->renderChildren($depth);
        return $out;
    }
}
